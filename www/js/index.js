let app = {
  config: {
    // https://derrupi.uber.space
    urlBase: "https://derrupi.uber.space/kinodb/api/api.php",
    token: "LN6Chh7qEPULmLTqD4GpKtAy9FPksv",
    movieQuery: "methode=getAllMovies",
    movieQueryDetail: "methode=getMovieDetail",
    movieID: null,
    baseAjaxOptions: {
      type: "GET",
      dataType: "json"
    },
    tabletLandscapeMinWidth: 800,
    isTabletLandscapeMode: false
  },

  onPageCreate() {
    app.registerEvents();
  },

  registerEvents() {
    $("#licenseSearchBtn").click(app.onLicenseSearchBtnClicked);
    $("#serialSearchBtn").click(app.onSerialSearchBtnClicked);
    $(".href").click(app.onClickHref);
  },

  hrefclick(){

    console.log("ok");
  },

  handleLayoutChange(event) {
    app.updateLayoutMode();

  },

  onClickHref() {
    app.config.movieID = null;
    console.log($(this));
    app.config.movieID = $(this)[0].getAttribute("title");
    console.log(app.config.movieID);
  },

  updateLayoutMode() {
    app.config.isTabletLandscapeMode = $(window).width() >= app.config.tabletLandscapeMinWidth;
  },

  onLoad(event) {
    event.preventDefault();
    app.getMovieData(app.config.token, app.config.movieQuery);
  },

  onLoadDetail(event){
    event.preventDefault();
    app.getMovieDetailData(app.config.token, app.config.movieQueryDetail, app.config.movieID);
  },

  getMovieDetailData(movieID, input, queryMethod) {
    let queryUrl = app.getDetailQueryUrl(app.config.token, app.config.movieQueryDetail, app.config.movieID);
    console.log(queryUrl);
    let ajaxOptions = $.extend({}, queryUrl, app.config.baseAjaxOptions);

    $.ajax(ajaxOptions)
      .done(app.onDetailAvailable)
      .fail(app.onRequestFailed);
  },

  getMovieData(input, queryMethod) {
    let queryUrl = app.getQueryUrl(app.config.token, queryMethod);

    let ajaxOptions = $.extend({}, queryUrl, app.config.baseAjaxOptions);

    $.ajax(ajaxOptions)
      .done(app.onResultAvailable)
      .fail(app.onRequestFailed);
  },

  getQueryUrl(token, queryMethod) {
    return {
      // Achtung: Keine normalen Anführungszeichen, sondern die über dem ü auf der Tastatur
      url: `${app.config.urlBase}?token=${token}&${queryMethod}`
    };
  },

  getDetailQueryUrl(token, queryMethod, movieID) {
    return {
      // Achtung: Keine normalen Anführungszeichen, sondern die über dem ü auf der Tastatur
      url: `${app.config.urlBase}?token=${token}&${queryMethod}&movie=${movieID}`
    };
  },

  onDetailAvailable(json) {
    console.log("onDetailAvailable");
    let html = app.parseDetailJson(json);
    console.log(html);
    $("#resultDetail").html(html);
  },

  parseDetailJson(json) {
    let html = "";
    console.log("parseDetailJson");
    for(let movie of json.movie){
        html += app.getHtmlForMovie(movie);
    }

    return html;
  },

  onResultAvailable(json) {
    let html = app.parseMovieJson(json);
    $("#result").html(html);
  },

  parseMovieJson(json) {
    let html = "";
    for(let movie of json.movie){
        html += app.getHtmlForMovie(movie);
    }

    return html;
  },

  getHtmlForMovie(movie) {
    console.log("getHtmlForMovie");
    return "<div id='movieoverview'>"+
          "<div id='poster'><img src='https://image.tmdb.org/t/p/w500"+ movie.poster +"' width='150px'></div>" +
          "<div id='content'>" +
            "<div id='movietitle'>" + movie.title + "</div>" +
            "<div id='overview'>" + movie.overview + "</div>" +
            "<div id='shows'>" + app.getHtmlForShow(movie.vorstellungen) + "</div>"+
            "<div id='link'><p><a class='href' href='#detail' title='" + movie.id + "'>Detailansicht</a></p>" +
            "</div>"+
          "</div>"+
          "</div>";
   },

  getHtmlForShow(shows){
    let html = "";
    for(let show of shows){
        html += "<div id='show'>" + show.uhrzeit + " (Kino "+ show.saalnr + ")</div>";
    }
    return html;
  },

  onRequestFailed(xhr, status, errorThrown) {
    alert("Ein Fehler ist aufgetreten");
    console.log(xhr);
    console.log(status);
    console.log(errorThrown);
  }
};
// $(document).on("pagebeforecreate", "#mainPage", app.handleLayoutChange);
// $(window).on("resize", app.handleLayoutChange);
$(document).on("pagecreate", "#mainPage", app.onLoad);
$(document).on("pagecreate", "#detail", app.onLoadDetail);
$(document).on("click", ".href", app.onClickHref);
$(document).on("pagecreate", "#mainPage", app.onPageCreate);
